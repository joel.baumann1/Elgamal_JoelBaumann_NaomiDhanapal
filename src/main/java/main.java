import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class main {
    //String hexString1 = "FFFFFFFF FFFFFFFF C90FDAA2 2168C234 C4C6628B 80DC1CD1 29024E08 8A67CC74 020BBEA6 3B139B22 514A0879 8E3404DD EF9519B3 CD3A431B 302B0A6D F25F1437 4FE1356D 6D51C245 E485B576 625E7EC6 F44C42E9 A637ED6B 0BFF5CB6 F406B7ED EE386BFB 5A899FA5 AE9F2411 7C4B1FE6 49286651 ECE45B3D C2007CB8 A163BF05 98DA4836 1C55D39A 69163FA8 FD24CF5F 83655D23 DCA3AD96 1C62F356 208552BB 9ED52907 7096966D 670C354E 4ABC9804 F1746C08 CA18217C 32905E46 2E36CE3B E39E772C 180E8603 9B2783A2 EC07A28F B5C55DF0 6F4C52C9 DE2BCBF6 95581718 3995497C EA956AE5 15D22618 98FA0510 15728E5A 8AACAA68 FFFFFFFF FFFFFFFF";
    public static final String localDir = System.getProperty("user.dir");
    public static final String hexStringN = "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF";
    public static final BigInteger g = BigInteger.TWO;
    public static final BigInteger n = new BigInteger(hexStringN,16);
    public static final BigInteger nCount = n.divide(BigInteger.valueOf(2048));
    public static final BigInteger b = BigInteger.valueOf((long)(Math.random()*2047));
    public static final BigInteger gb = g.pow(b.intValue());

    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        System.out.println("b= "+b);

        while(true) {


            System.out.println("Welche Aufgabe willst Du machen?(2,3,4 oder 5)");
            int scIn = scanner.nextInt();

            switch (scIn) {
            case 2:
                aufgabe2("pk.txt", "sk.txt");
                break;
            case 3:
                aufgabe3("pk.txt", "text.txt", "chiffre.txt");
                break;
            case 4:
                aufgabe4("chiffre.txt", "sk.txt", "text-d.txt");
                break;
            case 5:
                aufgabe5("chiffre.txt", "sk.txt");
                break;
            }
        }
   }



   public static void aufgabe2(String pkFile,String skFile) throws FileNotFoundException, UnsupportedEncodingException {
       writeTOFile(localDir+"\\"+skFile,b.toString());
       writeTOFile(localDir+"\\"+pkFile,gb.toString());
   }

   public static void aufgabe3(String pkFile,String textFile,String chiffreFile)throws IOException{

       String text = new String(Files.readAllBytes(Paths.get(localDir + "\\"+textFile)));
       String publicKeyText = new String(Files.readAllBytes(Paths.get(localDir + "\\"+pkFile)));
       BigInteger publikKey = new BigInteger(publicKeyText);
       ArrayList<BigInteger> asciiText = new ArrayList<BigInteger>();
       ArrayList<String> asciiTextEncoded = new ArrayList<String>();
       for(int i = 0;i<text.length();i++){
           asciiText.add(BigInteger.valueOf(text.charAt(i)));
       }
       System.out.println(asciiText);

       BigInteger a = BigInteger.valueOf((long)(Math.random()*2047));


       for(BigInteger big:asciiText){
           asciiTextEncoded.add("("+ g.pow(a.intValue()).toString()+","+nFunktion(gb.pow(a.intValue()),big,n).toString()+")");
       }
       System.out.println(asciiTextEncoded);

       writeTOFile(localDir+"\\"+chiffreFile,array2String(asciiTextEncoded));


   }

   public static void aufgabe4(String encodedTextFileName,String privKey,String textOutputFile) throws IOException {

        String result = decodeFile(encodedTextFileName,privKey);
        writeTOFile(localDir + "\\"+textOutputFile,result);

    }

   public static void aufgabe5(String encodedTextFileName,String privKey) throws IOException {

       System.out.println(decodeFile(encodedTextFileName,privKey));


   }

   public static String decodeFile(String encodedTextFileName, String keyFileName)throws IOException{
       String decodeThis = new String(Files.readAllBytes(Paths.get(localDir + "\\"+ encodedTextFileName)));
       String[] decodeThis2 = decodeThis.split(";");
       String outputDecoded = "";
       String bFromSK = new String(Files.readAllBytes(Paths.get(localDir + "\\" + keyFileName)));
       BigInteger bNew = new BigInteger(bFromSK);


       for (String s : decodeThis2){
           String[] st = s.substring(1, s.length()-1).split(",");
           BigInteger y1 = new BigInteger(st[0]);
           BigInteger y2 = new BigInteger(st[1]);
           BigInteger gab = y1.modPow(bNew,n);
           BigInteger gab1 = gab.modInverse(n);
           char c = (char)nFunktion(y2,gab1,n).intValue();
           outputDecoded +=c;
       }
        return outputDecoded;
   }


    public static BigInteger nFunktion(BigInteger a, BigInteger b, BigInteger n){

       return a.multiply(b).mod(n);

    }


    public static void writeTOFile(String filename, String writeThis)throws FileNotFoundException,
        UnsupportedEncodingException {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(filename), "utf-8"))) {
            writer.write(writeThis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String array2String(ArrayList<String> takeThis){
        String result = "";
        for(int i = 0;i<takeThis.size();i++){
            if(i>0){
                result += ";";
            }
            result += takeThis.get(i);
        }
        return result;
    }

}
